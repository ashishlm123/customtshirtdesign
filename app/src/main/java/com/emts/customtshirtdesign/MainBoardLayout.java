package com.emts.customtshirtdesign;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.customtshirtdesign.helper.AlertUtils;
import com.emts.customtshirtdesign.helper.Api;
import com.emts.customtshirtdesign.helper.Logger;
import com.emts.customtshirtdesign.helper.NetworkUtils;
import com.emts.customtshirtdesign.helper.VolleyHelper;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Random;

import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * Created by User on 2017-02-07.
 */

public class MainBoardLayout extends AppCompatActivity {
    ImageView addText, addStickers, pickColor, deleteView;
    FrameLayout mainBoard, backMainBoard;
    ImageView ivShirtView;
    ImageView ivChooseShirt, ivChooseClip, ivChooseShape, ivFlipShirt, ivBackshirt;
    ImageView editText;
    ImageView imageView;
    RecyclerView shirtListView, clipListView, shapeListView;
    ShirtListAdapter shirtAdapter;
    ClipListAdapter clipAdapter;
    ShapeAdapter shapeAdapter;
    TextView tvLvl;
    boolean isBack = false;


    //    TextView selectedTextView;
    int selectedColor = Color.BLACK;
    int shapeColor = Color.BLUE;
    int borderColor = Color.RED;
    View selectedColorPreview, shapeColorPreview, strokeColorPreview;
    //    ImageView selectedImageView;
    View selectedView;


    public static final int DATA_STORAGE = 80;

    RelativeLayout holderPickColor, holderShapeColor, holderStrokeColor;
    LinearLayout holderTextControls, holderImageControls, holderShapeControls;
    SeekBar sbImageSize, sbTvSize, sbShapeSize, sbStrokeWidth;

    private int textSize = 20;

    public static final int RESULT_LOAD_IMAGE = 16;
    private static final String IMAGE_DIRECTORY_NAME = "CustomShirt";

    // Camera activity request codes
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static Uri fileUri;

    float discrete = 0;
    float start = 0;
    float end = 100;
    float start_pos = 0;
    int start_position = 0;

    ArrayList<Art> shirtList = new ArrayList<>();
    ArrayList<Art> clipList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main_board_layout);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("theone");
        toolbar.inflateMenu(R.menu.menu_main);


        ivShirtView = (ImageView) findViewById(R.id.iv_shirt);
        backMainBoard = (FrameLayout) findViewById(R.id.holder_back_mockup);
        ivBackshirt = (ImageView) findViewById(R.id.iv_shirt_back);
        ivFlipShirt = (ImageView) findViewById(R.id.iv_flip_sides);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (selectedView != null) {
                    selectedView.setBackgroundResource(0);
                }

                if (ContextCompat.checkSelfPermission(MainBoardLayout.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    ActivityCompat.requestPermissions(MainBoardLayout.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            DATA_STORAGE);
                } else {
                    Bitmap mockUp = getBitmapOfMockup(mainBoard);
                    String imageUri = saveBitmapToSdCard(mockUp);
                    Bitmap mockUp2 = getBitmapOfMockup(backMainBoard);
                    String imageUri2 = saveBitmapToSdCard(mockUp2);
                    Intent intent = new Intent(getApplicationContext(), PreviewActivity.class);
                    intent.putExtra("image", imageUri);
                    intent.putExtra("image_back", imageUri2);
                    startActivity(intent);

//                    new ImageLoadTask().execute();
                }


                return false;
            }
        });

        addText = (ImageView) findViewById(R.id.add_text);
        addStickers = (ImageView) findViewById(R.id.add_image);
        holderPickColor = (RelativeLayout) findViewById(R.id.holder_pick_color);
        pickColor = (ImageView) findViewById(R.id.add_color);
        selectedColorPreview = findViewById(R.id.view_selected_color);
        previewColorSelected(selectedColor, selectedColorPreview);
        sbShapeSize = (SeekBar) findViewById(R.id.sb_shape_size);

        sbStrokeWidth = (SeekBar) findViewById(R.id.sb_stroke_size);


        shapeColorPreview = findViewById(R.id.view_shape_color);
        previewColorSelected(shapeColor, shapeColorPreview);

        strokeColorPreview = findViewById(R.id.view_stroke_color);
        previewColorSelected(borderColor, strokeColorPreview);

        editText = (ImageView) findViewById(R.id.edit_text);

        holderShapeColor = (RelativeLayout) findViewById(R.id.holder_shape_color);
        holderStrokeColor = (RelativeLayout) findViewById(R.id.holder_stroke_color);
        //ADDITIONAL CONTROLS
        holderTextControls = (LinearLayout) findViewById(R.id.holder_additional_controls_text);
        holderImageControls = (LinearLayout) findViewById(R.id.holder_additional_controls_image);
        holderShapeControls = (LinearLayout) findViewById(R.id.holder_additional_controls_shape);
        shirtListView = (RecyclerView) findViewById(R.id.shirt_design_list);
        shirtListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        shirtAdapter = new ShirtListAdapter(this, shirtList);
        shirtAdapter.setOnRecyclerViewItemClickListener(new ShirtListAdapter.OnRecyclerViewItemClicked() {
            @Override
            public void onRecyclerViewItemClick(int position) {
                Picasso.with(MainBoardLayout.this).load(String.valueOf(shirtList.get(position).getFrontImagePath())).into(ivShirtView);
                Picasso.with(MainBoardLayout.this).load(String.valueOf(shirtList.get(position).getBackImagePath())).into(ivBackshirt);


            }
        });
        shirtListView.setAdapter(shirtAdapter);
        if (NetworkUtils.isInNetwork(this)) {
            tshirtListingTask();
            clipListingTask();
        } else {
            AlertUtils.showSnack(MainBoardLayout.this, toolbar, "No Internet Connected");
        }

        clipListView = (RecyclerView) findViewById(R.id.clip_art_list);
        clipListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        clipAdapter = new ClipListAdapter(this, clipList);
        clipAdapter.setOnRecyclerViewItemClickListener(new ClipListAdapter.OnRecyclerViewItemClicked() {
            @Override
            public void onRecyclerViewItemClick(int position) {
                addClip(position);
            }
        });
        clipListView.setAdapter(clipAdapter);

        shapeListView = (RecyclerView) findViewById(R.id.shape_list);
        shapeListView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        shapeAdapter = new ShapeAdapter(this);
        shapeAdapter.setOnRecyclerViewItemClickListener(new ShapeAdapter.OnRecyclerViewItemClicked() {
            @Override
            public void onRecyclerViewItemClick(int position) {
                addShape(position);

            }
        });
        shapeListView.setAdapter(shapeAdapter);


        ivChooseShirt = (ImageView) findViewById(R.id.iv_choose_shirt);
        ivChooseShirt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shirtListView.getVisibility() == View.VISIBLE) {
                    closeShirtChooserList();
                } else {
                    showShirtChooserList();
                }
            }
        });

        mainBoard = (FrameLayout) findViewById(R.id.holder_mockup);
        ivShirtView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (selectedView != null) {
                        selectedView.setBackgroundResource(0);
                        selectedView = null;
                    }
                    if (shirtListView.getVisibility() == View.VISIBLE) {
                        closeShirtChooserList();
                    }
                    if (shapeListView.getVisibility() == View.VISIBLE) {
                        closeShapeChooserList();
                    }
//                    closeHolderImageControls();
//                    closeHolderTextControls();

                    hideHolderControl(holderImageControls);
                    hideHolderControl(holderTextControls);
                    hideHolderControl(holderShapeControls);

                    if (clipListView.getVisibility() == View.VISIBLE) {
                        closeClipChooserList();
                    }
                }
                return true;
            }
        });
        ivBackshirt.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (selectedView != null) {
                        selectedView.setBackgroundResource(0);
                        selectedView = null;
                    }
                    if (shirtListView.getVisibility() == View.VISIBLE) {
                        closeShirtChooserList();
                    }
                    if (shapeListView.getVisibility() == View.VISIBLE) {
                        closeShapeChooserList();
                    }
//                    closeHolderImageControls();
//                    closeHolderTextControls();

                    hideHolderControl(holderImageControls);
                    hideHolderControl(holderTextControls);
                    hideHolderControl(holderShapeControls);

                    if (clipListView.getVisibility() == View.VISIBLE) {
                        closeClipChooserList();
                    }
                }
                return true;
            }
        });

        ivFlipShirt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mainBoard.getVisibility() == View.VISIBLE) {
                    isBack = true;
                    backMainBoard.setVisibility(View.VISIBLE);
                    mainBoard.setVisibility(View.GONE);
                } else {
                    isBack = false;
                    backMainBoard.setVisibility(View.GONE);
                    mainBoard.setVisibility(View.VISIBLE);
                }

            }
        });
        ivChooseClip = (ImageView) findViewById(R.id.iv_choose_clipart);
        ivChooseClip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clipListView.getVisibility() == View.VISIBLE) {
                    closeClipChooserList();
                } else {
                    showClipChooserList();
                }
            }
        });

        ivChooseShape = (ImageView) findViewById(R.id.iv_choose_shapes);
        ivChooseShape.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shapeListView.getVisibility() == View.VISIBLE) {
                    closeShapeChooserList();
                } else {
                    showShapeChooserList();
                }

            }
        });


        deleteView = (ImageView) findViewById(R.id.delete);
        deleteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedView != null) {
                    if (selectedView instanceof TextView) {
                        if (isBack) {
                            backMainBoard.removeView(selectedView);
                        } else {
                            mainBoard.removeView(selectedView);
                        }
//                        closeHolderTextControls();
                        hideHolderControl(holderTextControls);
                    } else if (selectedView instanceof ImageView) {
                        if (isBack) {
                            backMainBoard.removeView(selectedView);
                        } else {
                            mainBoard.removeView(selectedView);
                        }
//                        closeHolderImageControls();
                        Art art = (Art) selectedView.getTag();
                        if (art.getArtType().equals(Art.SHAPE)) {
                            hideHolderControl(holderShapeControls);
                        } else {
                            hideHolderControl(holderImageControls);
                        }

                    }
                }
            }
        });


        addText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                selectView(view);
                addnewText("");

            }
        });

        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedView != null && selectedView instanceof TextView) {
                    addnewText(((TextView) selectedView).getText().toString().trim());
                }

            }
        });


        addStickers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectImage();
//                selectView(view);
            }
        });
        holderPickColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectView(view);
                AmbilWarnaDialog dialog = new AmbilWarnaDialog(MainBoardLayout.this, selectedColor,
                        new AmbilWarnaDialog.OnAmbilWarnaListener() {
                            @Override
                            public void onCancel(AmbilWarnaDialog dialog) {
                            }

                            @Override
                            public void onOk(AmbilWarnaDialog dialog, int color) {
                                selectedColor = color;
                                previewColorSelected(selectedColor, selectedColorPreview);
                                if (selectedView instanceof TextView) {
                                    tvLvl.setTextColor(selectedColor);
                                }
                            }
                        });
                dialog.show();
            }
        });

        holderShapeControls.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AmbilWarnaDialog dialog = new AmbilWarnaDialog(MainBoardLayout.this, shapeColor,
                        new AmbilWarnaDialog.OnAmbilWarnaListener() {
                            @Override
                            public void onCancel(AmbilWarnaDialog dialog) {
                            }

                            @Override
                            public void onOk(AmbilWarnaDialog dialog, int color) {
                                shapeColor = color;
                                previewColorSelected(shapeColor, shapeColorPreview);

                                if (selectedView instanceof ImageView) {
                                    Art art = (Art) selectedView.getTag();
                                    Drawable draw = drawShape(art.getShape(), shapeColor, art.getBorderColor(), art.getBorderwidth());
                                    imageView.setImageDrawable(draw);
                                    art.setShapeColor(color);
                                    selectedView.setTag(art);

                                }
                            }
                        });
                dialog.show();
            }
        });

        holderStrokeColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AmbilWarnaDialog dialog = new AmbilWarnaDialog(MainBoardLayout.this, borderColor,
                        new AmbilWarnaDialog.OnAmbilWarnaListener() {
                            @Override
                            public void onCancel(AmbilWarnaDialog dialog) {
                            }

                            @Override
                            public void onOk(AmbilWarnaDialog dialog, int color) {
                                borderColor = color;
                                previewColorSelected(borderColor, strokeColorPreview);

                                if (selectedView instanceof ImageView) {
                                    Art art = (Art) selectedView.getTag();
                                    Drawable draw = drawShape(art.getShape(), art.getShapeColor(), borderColor, art.getBorderwidth());
                                    imageView.setImageDrawable(draw);
                                    art.setBorderColor(borderColor);
                                    selectedView.setTag(art);

                                }
                            }
                        });
                dialog.show();
            }
        });


        //text controls
        sbTvSize = (SeekBar) findViewById(R.id.sb_text_size);
        sbTvSize.setProgress(40);
        sbTvSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                textSize = i;
                Log.e("prog", String.valueOf(textSize));
//                if (selectedTextView != null) {
//                    selectedTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
//                    selectedTextView.invalidate();
//                }
                Art art = (Art) selectedView.getTag();
                if (selectedView != null && selectedView instanceof TextView) {
                    if (textSize == 0) {
                        ((TextView) selectedView).setTextSize(TypedValue.COMPLEX_UNIT_SP, 1);
                    } else {
                        ((TextView) selectedView).setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
                    }
                    art.setTextSize(textSize);
                    selectedView.setTag(art);
                    selectedView.invalidate();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        SeekBar sbTvRotate = (SeekBar) findViewById(R.id.sb_text_rotate);
        sbTvRotate.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (selectedView != null && selectedView instanceof TextView) {
//                    rotateViewTask(i);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Spinner spTextStyle = (Spinner) findViewById(R.id.sp_text_style);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainBoardLayout.this, R.layout.item_spinner, getResources().getStringArray(R.array.text_style));
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTextStyle.setAdapter(adapter);
        spTextStyle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (selectedTextView != null) {
//                    switch (i) {
//                        case 0:
////                            selectedTextView.setTypeface(selectedTextView.getTypeface(), Typeface.NORMAL);
//                            selectedTextView.setTypeface(Typeface.create(selectedTextView.getTypeface(), Typeface.NORMAL));
//                            break;
//                        case 1:
//                            selectedTextView.setTypeface(selectedTextView.getTypeface(), Typeface.BOLD);
//                            break;
//                        case 2:
//                            selectedTextView.setTypeface(selectedTextView.getTypeface(), Typeface.ITALIC);
//                            break;
//                        case 3:
//                            selectedTextView.setTypeface(selectedTextView.getTypeface(), Typeface.BOLD_ITALIC);
//                            break;
//                    }
//                }
                if (selectedView != null && selectedView instanceof TextView) {
                    switch (i) {
                        case 0:
                            ((TextView) selectedView).setTypeface(Typeface.create(((TextView) selectedView).getTypeface(), Typeface.NORMAL));
                            break;
                        case 1:
                            ((TextView) selectedView).setTypeface(Typeface.create(((TextView) selectedView).getTypeface(), Typeface.BOLD));
                            break;
                        case 2:
                            ((TextView) selectedView).setTypeface(Typeface.create(((TextView) selectedView).getTypeface(), Typeface.ITALIC));
                            break;
                        case 3:
                            ((TextView) selectedView).setTypeface(Typeface.create(((TextView) selectedView).getTypeface(), Typeface.BOLD_ITALIC));
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });


        //image controls
        sbImageSize = (SeekBar) findViewById(R.id.sb_iamge_size);
        start_pos = 50;
        start_position = (int) (((start_pos - start) / (end - start)) * 100);
        discrete = start_pos;
        sbImageSize.setProgress(start_position);
        sbImageSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                float temp = progress;
                float dis = end - start;
                discrete = (start + ((temp / 100) * dis));

                Log.e("prog", String.valueOf(discrete));
                int mindimention = 200;
                Art art = (Art) selectedView.getTag();

                if (art.getArtType().equals(Art.CLIP)) {
//                    Bitmap bitmap = resizeClip(((int)discrete*mindimention)/50, (int) selectedView.getTag());
                    Bitmap bitmap = art.getClip();
                    int height = bitmap.getHeight();
                    int width = bitmap.getWidth();

                    Logger.e("wid", String.valueOf(height) + " " + String.valueOf(width));

                    if (clipListView.getVisibility() == View.VISIBLE) {
                        closeClipChooserList();
                    }
                    if (discrete == 0) {
                        bitmap = resizeClip(mindimention / 50, bitmap, height, width);
                        ((ImageView) selectedView).setImageBitmap(bitmap);
                        art.setImageWidth(bitmap.getWidth());
                        art.setImageHeight(bitmap.getHeight());


                    } else {

                        bitmap = resizeClip(((int) discrete * mindimention) / 50, bitmap, height, width);
                        ((ImageView) selectedView).setImageBitmap(bitmap);
                        art.setImageWidth(bitmap.getWidth());
                        art.setImageHeight(bitmap.getHeight());
                    }

                } else if (art.getArtType().equals(Art.IMAGE)) {
                    if (discrete == 0) {
                        mindimention = 200;
                        if (selectedView != null) {
                            Bitmap newBm = resizeHeightWidth(art.getImagepath(),
                                    (int) (mindimention * 0.01));
                            art.setImageWidth(newBm.getWidth());
                            art.setImageHeight(newBm.getHeight());
                            ((ImageView) selectedView).setImageBitmap(newBm);
                        }
                    } else {
                        mindimention = (int) ((mindimention * discrete) / 50);
                        if (selectedView != null && selectedView instanceof ImageView) {
                            Bitmap newBm = resizeHeightWidth(art.getImagepath(), mindimention);
                            art.setImageWidth(newBm.getWidth());
                            art.setImageHeight(newBm.getHeight());
                            ((ImageView) selectedView).setImageBitmap(newBm);
                        }
                    }
                }

                selectedView.setTag(art);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        sbShapeSize.setProgress(50);
        sbShapeSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                Log.e("progress", String.valueOf(progress));
                Art art = (Art) selectedView.getTag();
                Log.e("progress0", String.valueOf(art.getImageHeight()) + "  " + String.valueOf(art.getImageWidth()));
                if (progress == 0) {
                    int height = 5;
                    int width = 5;
                    Log.e("At progress 0", String.valueOf(height) + "  " + String.valueOf(width));
                    FrameLayout.LayoutParams layoutParam = new FrameLayout.LayoutParams(width, height);
                    layoutParam.gravity = Gravity.CENTER;
                    (selectedView).setLayoutParams(layoutParam);
                    art.setImageWidth(width);
                    art.setImageHeight(height);
                    selectedView.setTag(art);
                } else {
                    Log.e("previous height", String.valueOf(art.getImageHeight()) + "   " + String.valueOf(art.getImageWidth()));
                    Double discrete = Math.ceil(progress / 50);
                    Log.e("discrete", String.valueOf(discrete));
//
//                    int height = art.getImageHeight() * Integer.valueOf(String.valueOf(discrete));
//                    int width = art.getImageWidth() * Integer.valueOf(String.valueOf(discrete));
                    int height = 4 * progress;
                    int width = 4 * progress;
                    Log.e("change in height", String.valueOf(height) + String.valueOf(width));
                    FrameLayout.LayoutParams layoutParam = new FrameLayout.LayoutParams(width, height);
                    layoutParam.gravity = Gravity.CENTER;
                    (selectedView).setLayoutParams(layoutParam);
                    art.setImageWidth(width);
                    art.setImageHeight(height);
                    selectedView.setTag(art);
                }


            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        sbStrokeWidth.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                int width;
                Art art = (Art) selectedView.getTag();
                if (progress == 0) {
                    width = 1;
                } else {
                    width = progress / 10;
                }
                Drawable drawable = drawShape(art.getShape(), art.getShapeColor(), art.getBorderColor(), width);
                imageView.setImageDrawable(drawable);
                art.setBorderwidth(width);
                selectedView.setTag(art);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        SeekBar sbIvRotation = (SeekBar) findViewById(R.id.sb_image_roation);
        sbIvRotation.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                if (selectedView != null && selectedView instanceof ImageView) {
//                    rotateViewTask(i);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

    }

    private void clipListingTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = new HashMap<>();

        final ProgressDialog pDialog = new ProgressDialog(MainBoardLayout.this);
        pDialog.setMessage("Please Wait..");
        pDialog.show();
        vHelper.addVolleyRequestListeners(Api.getInstance().getClipArtUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                Logger.e("response", response);
                try {
                    JSONObject resObj = new JSONObject(response);

                    if (resObj.getBoolean("status")) {
                        JSONArray array = resObj.getJSONArray("tshirt");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject eachObj = array.getJSONObject(i);
                            Art art = new Art();
                            art.setClientId(Integer.parseInt(eachObj.getString("client_id")));
                            art.setImagepath(resObj.getString("img_dir") + eachObj.getString("art_name"));
                            Logger.e("clippath", String.valueOf(resObj.getString("img_dir") + eachObj.getString("art_name")));
                            clipList.add(art);
                        }
                        clipAdapter.notifyDataSetChanged();
                    } else {
                        AlertUtils.showSnack(MainBoardLayout.this, addText, resObj.getString("message"));
                    }


                } catch (JSONException e) {
                    Logger.e("clipListingTask ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    Logger.e("clipListingTask error", errorObj.getString("message"));

                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), "No Internet connected", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), "server Error", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), "Time out error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "clipListingTask");
    }

    private void tshirtListingTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = new HashMap<>();

        final ProgressDialog pDialog = new ProgressDialog(MainBoardLayout.this);
        pDialog.setMessage("Please Wait..");
        pDialog.show();
        vHelper.addVolleyRequestListeners(Api.getInstance().getTShirtUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                Logger.e("response", response);
                try {
                    JSONObject resObj = new JSONObject(response);

                    if (resObj.getBoolean("status")) {
                        JSONArray array = resObj.getJSONArray("tshirt");
                        for (int i = 0; i < array.length(); i++) {
                            JSONObject eachObj = array.getJSONObject(i);
                            Art art = new Art();
                            art.setCatName(eachObj.getString("cat_name"));
                            art.setFrontImagePath(resObj.getString("img_dir") + eachObj.getString("front_image_name"));
                            art.setBackImagePath(resObj.getString("img_dir") + eachObj.getString("back_image_name"));
                            Logger.e("imagepath", String.valueOf(resObj.getString("img_dir") + eachObj.getString("front_image_name")));
                            shirtList.add(art);
                        }
                        shirtAdapter.notifyDataSetChanged();
                    } else {
                        AlertUtils.showSnack(MainBoardLayout.this, tvLvl, resObj.getString("message"));
                    }


                } catch (JSONException e) {
                    Logger.e("tshirtListingTask ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    AlertUtils.showSnack(MainBoardLayout.this, tvLvl, errorObj.getString("message"));

                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), "No Internet connected", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), "server Error", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), "Authentication failed", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), "Parse Error", Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), "Time out error", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "tshirtListingTask");
    }

    private void showShapeChooserList() {
        Animation showAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.show_shape);
        shapeListView.startAnimation(showAnim);
        shapeListView.setVisibility(View.VISIBLE);
    }

    private void closeShapeChooserList() {
        Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.hide_shape);
        shapeListView.startAnimation(hideAnim);
        shapeListView.setVisibility(View.GONE);
    }

    private void addShape(int position) {
        int height = 0;
        int width = 0;
        imageView = new ImageView(MainBoardLayout.this);
        GradientDrawable drawable = null;
        int backColor = getResources().getColor(R.color.colorAccent);
        int borderColor = getResources().getColor(R.color.colorPrimary);
        int bwidth = 5;
        Art shape = new Art();


        if (position == 0) {
            height = 200;
            width = 200;
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height);
            lp.gravity = Gravity.CENTER;
            imageView.setLayoutParams(lp);
            drawable = drawShape("rec", backColor, borderColor, bwidth);
            imageView.setImageDrawable(drawable);
            shape.setShape("rec");
        }
        if (position == 1) {
            height = 50;
            width = 200;
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height);
            lp.gravity = Gravity.CENTER;
            imageView.setLayoutParams(lp);
            drawable = drawShape("rec", backColor, borderColor, bwidth);
            imageView.setImageDrawable(drawable);
            shape.setShape("rec");
        } else if (position == 2) {
            height = 200;
            width = 200;
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height);
            lp.gravity = Gravity.CENTER;
            imageView.setLayoutParams(lp);
            drawable = drawShape("cir", backColor, borderColor, bwidth);
            imageView.setImageDrawable(drawable);
            shape.setShape("cir");

        } else if (position == 3) {
            height = 50;
            width = 200;
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height);
            lp.gravity = Gravity.CENTER;
            imageView.setLayoutParams(lp);
            drawable = drawShape("cir", backColor, borderColor, bwidth);
            imageView.setImageDrawable(drawable);
            shape.setShape("cir");
        } else if (position == 4) {
            height = 200;
            width = 200;
            FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(width, height);
            lp.gravity = Gravity.CENTER;
            imageView.setLayoutParams(lp);
            drawable = drawShape("", backColor, borderColor, bwidth);
            imageView.setImageDrawable(drawable);
            shape.setShape("");
        }
        imageView.setTag(drawable);
        imageView.setOnTouchListener(touchListener);
        if (selectedView != null) {
            selectedView.setBackgroundResource(0);
        }
//        imageView.setTag("Shape");
        selectedView = imageView;
        selectedView.setBackgroundResource(R.drawable.bg_selection);
//        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicHeight(), drawable.getIntrinsicWidth(), Bitmap.Config.ARGB_8888);

        shape.setArtType(Art.SHAPE);
        shape.setDrawable(drawable);
        shape.setBorderColor(borderColor);
        shape.setShapeColor(backColor);
        shape.setImageHeight(height);
        shape.setImageWidth(width);
        selectedView.setTag(shape);

        ((ImageView) selectedView).setImageDrawable(drawable);
        if (isBack) {
            backMainBoard.addView(selectedView);
        } else {
            mainBoard.addView(selectedView);
        }


    }


    GradientDrawable drawShape(String shape, int color, int borderColor, int borderWidth) {

        GradientDrawable thumb = new GradientDrawable(
                GradientDrawable.Orientation.TOP_BOTTOM,
                new int[]{color, color});
        thumb.setStroke(borderWidth, borderColor);

        if (shape.equals("rec")) {
            thumb.setShape(GradientDrawable.RECTANGLE);
        } else if (shape.equals("cir")) {
            thumb.setShape(GradientDrawable.OVAL);
        } else {
            thumb.setShape(GradientDrawable.LINE);
        }


        return thumb;

    }

    private Bitmap resizeClip(int dimen, Bitmap bitmaps, int height, int width) {
        int minDimen = dimen;

        Bitmap bitmap;

        // create the options
        BitmapFactory.Options opts = new BitmapFactory.Options();

        //just decode the file
        opts.inJustDecodeBounds = true;
//        BitmapFactory.decodeResource(bitmaps);


        //get the original size
//        int orignalHeight = opts.outHeight;
//        int orignalWidth = opts.outWidth;
//
        int orignalHeight = height;
        int orignalWidth = width;

        Log.e("original height:width", orignalHeight + "::" + orignalWidth);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        bitmap = bitmaps;

        float newHeight = orignalHeight;
        float newWidth = orignalWidth;
        //change height of image to 200 if more than that
        if (orignalHeight > minDimen) {
            newHeight = minDimen;
            final float heightRatio = (float) orignalHeight / (float) minDimen;
            newWidth = newWidth / heightRatio;
            Log.e("newWidth for height 600", newWidth + " *" + "hei" + newHeight);

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
//            Log.e("resize height is greater with ratio :", heightRatio + "New Height: " + bitmap.getHeight()
//                    + " New Width: " + bitmap.getWidth() + " ");
        }
        if (newWidth > minDimen) {
            final float widthRatio = (float) newWidth / (float) minDimen;
            newWidth = minDimen;
            newHeight = newHeight / widthRatio;

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
            Log.e("newHeight for width 600", newWidth + " *" + "heig" + newHeight);

            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight());//, matrix, true);

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
//            Log.e("resize width is greater with ratio :", " " + widthRatio + "New Height: " + bitmap.getHeight() + " New Width: " + bitmap.getWidth());
        }
        return bitmap;
    }


    private void showClipChooserList() {
        Animation showAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.right);
        clipListView.startAnimation(showAnim);
        clipListView.setVisibility(View.VISIBLE);
    }

    private void closeClipChooserList() {
        Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.left);
        clipListView.startAnimation(hideAnim);
        clipListView.setVisibility(View.GONE);
    }

//    private void rotateViewTask(int progress) {
//
//        float difference = 0;
//
//        if (progress > 0) {
//            float temp;
//            temp = (int) (3.6 * progress);
//            difference = temp - start;
//            start = temp;
//
//            Log.e("difference", String.valueOf(difference));
//
////text rotation
//            RotateAnimation rotate = new RotateAnimation(start, temp,
//                    Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
//            rotate.setInterpolator(new LinearInterpolator());
//
////                      //prevents View from restoring to original direction.
//            rotate.setFillAfter(true);
//
//
//            if (selectedView != null && selectedView instanceof ImageView) {
//                imageView.startAnimation(rotate);
//            }
//
//
//            if (selectedView != null && selectedView instanceof TextView) {
//                tvLvl.startAnimation(rotate);
//            }
//
//
//        }
//    }


//    private class ImageLoadTask extends AsyncTask<String, Void, String> {
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//        }
//
//        @Override
//        protected String doInBackground(String... strings) {
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//
//                }
//            });
//
//            Bitmap mockUp = getBitmapOfMockup(mainBoard);
//            String imageUri = saveBitmapToSdCard(mockUp);
//
//            return imageUri;
//        }
//
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            Intent intent = new Intent(getApplicationContext(), PreviewActivity.class);
//            intent.putExtra("image", s);
//            startActivity(intent);
//        }
//    }


    private void addnewText(final String text) {

        final AlertDialog.Builder alert = new AlertDialog.Builder(MainBoardLayout.this);
        alert.setTitle("Add text");
        final EditText etCustomText = new EditText(MainBoardLayout.this);
        etCustomText.setHint("Enter text to add");


        FrameLayout container = new FrameLayout(MainBoardLayout.this);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.rightMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margin);
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margintop);
        params.bottomMargin = getResources().getDimensionPixelSize(R.dimen.dialog_margintop);
        etCustomText.setLayoutParams(params);
        container.addView(etCustomText);

        if (!TextUtils.isEmpty(text)) {
            etCustomText.setText(text);
        }
        alert.setView(container);
        alert.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!TextUtils.isEmpty(etCustomText.getText().toString().trim())) {
                    if (!TextUtils.isEmpty(text)) {
                        ((TextView) selectedView).setText(etCustomText.getText().toString().trim());

                    } else {
                        addLevelView(etCustomText.getText().toString().trim(), 50);
                    }
                } else {
                    Toast.makeText(MainBoardLayout.this, "Enter some text", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Log.e("dismis", "u r here222");
                if (selectedView == null || !(selectedView instanceof TextView)) {
//                    closeHolderTextControls();
                    hideHolderControl(holderTextControls);
                    Log.e("edit", "u r here");
                }
            }
        });

        alert.show();
    }


    @Override
    public void onBackPressed() {
        if (shirtListView.getVisibility() == View.VISIBLE) {
            closeShirtChooserList();
            return;
        }
        super.onBackPressed();
    }

    private void showShirtChooserList() {
        Animation showAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
        shirtListView.startAnimation(showAnim);
        shirtListView.setVisibility(View.VISIBLE);
    }

    private void closeShirtChooserList() {
        Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
        shirtListView.startAnimation(hideAnim);
        shirtListView.setVisibility(View.GONE);
    }

    private void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(MainBoardLayout.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals("Take Photo")) {

                    if (ContextCompat.checkSelfPermission(MainBoardLayout.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(MainBoardLayout.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                                CAMERA_CAPTURE_IMAGE_REQUEST_CODE);


                    } else {
                        //THIS INTENT OPENS APPLICATION THAT CAN CAPTURE IMAGE i.e.  CAMERA APP
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

                        // YOU SHOULD PROVIDE THE LOCATION TO STORE THE CAPTURED IMAGE
                        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);

                        //SEE HERE extra_output TO IS fileuri THAT IS LOCATION TO SAVE CAPTURED IMAGE
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                        //NOTE startActivityForResult  THIS MEANS THE INTENT (camera) THAT WE ARE GOING TO OPEN WILL RETURN US A RESULT SO WE WILL CATCH THE RESULT IN onActivityresult
                        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                    }
                } else if (options[item].equals("Choose from Gallery")) {
                    // get permission dialog if permission not granted
                    //THIS IS FOR MARSHMALLOW AS IT REQUIRES PERMISSION IN RUN TIME
                    if (ContextCompat.checkSelfPermission(MainBoardLayout.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(MainBoardLayout.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                RESULT_LOAD_IMAGE);
                    } else {
                        //THIS INTENT IS TO PICK IMAGE SO THE APPLICATION THAT CAN HANDLE IMAGE PICK WILL HANDLE THIS INTENT i.e. gallery will open
                        Intent i = new Intent(
                                Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, RESULT_LOAD_IMAGE);

                    }
                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });


        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                Log.e("dismis", "u r here222");
                if (selectedView == null || !(selectedView instanceof ImageView)) {
//                    closeHolderImageControls();
                    hideHolderControl(holderImageControls);
                    Log.e("edit", "u r here");
                }
            }
        });
        builder.show();
    }

    private Bitmap getBitmapOfMockup(View v) {
//        Bitmap b = Bitmap.createBitmap( v.getLayoutParams().width, v.getLayoutParams().height, Bitmap.Config.ARGB_8888);
//        Canvas c = new Canvas(b);
//        v.layout(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
//        v.draw(c);
//        return b;
        if (v.getMeasuredHeight() <= 0) {
            v.measure(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        }
        Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_4444);


        Canvas c = new Canvas(b);
        c.drawColor(getResources().getColor(android.R.color.white));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        v.draw(c);
        return b;
    }

    private String saveBitmapToSdCard(Bitmap bitmap) {
        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/saved_images");
        myDir.mkdirs();
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "CS-" + n + ".png";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();

            //By using this line you can able to see saved images in the gallery view.
            sendBroadcast(new Intent(
                    Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));

            Toast.makeText(getApplicationContext(), "Image Saved !!!", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return file.getPath();
    }

    public void selectView(View view) {
        switch (view.getId()) {
            case R.id.add_text:
                Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
                holderTextControls.startAnimation(hideAnim);
                holderTextControls.setVisibility(View.VISIBLE);

                addText.setBackgroundColor(getResources().getColor(R.color.grey1));

                addStickers.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                holderPickColor.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                Animation hideAnim3 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
                holderImageControls.startAnimation(hideAnim3);
                holderImageControls.setVisibility(View.GONE);
                break;
            case R.id.add_image:
                Animation hideAnim2 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
                holderImageControls.startAnimation(hideAnim2);
                holderImageControls.setVisibility(View.VISIBLE);
                addStickers.setBackgroundColor(getResources().getColor(R.color.grey1));

                addText.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                holderPickColor.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                Animation hideAnim4 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
                holderTextControls.startAnimation(hideAnim4);
                holderTextControls.setVisibility(View.GONE);
                break;
            case R.id.holder_pick_color:
                holderPickColor.setBackgroundColor(getResources().getColor(R.color.grey1));

                addText.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                addStickers.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                holderTextControls.setVisibility(View.GONE);
                holderImageControls.setVisibility(View.GONE);
                break;
        }
    }

    private void addLevelView(String label, int textSize) {
        tvLvl = new TextView(MainBoardLayout.this);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        tvLvl.setLayoutParams(lp);
        tvLvl.setPadding(0, 0, 0, 0);
        tvLvl.setText(label);
        tvLvl.setTextColor(selectedColor);
        tvLvl.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        tvLvl.setOnTouchListener(touchListener);

        if (isBack) {
            backMainBoard.addView(tvLvl);
        } else {
            mainBoard.addView(tvLvl);
        }
//        selectedTextView = tvLvl;
        if (selectedView != null) {
            selectedView.setBackgroundResource(0);
        }
        selectedView = tvLvl;
        selectedView.setBackgroundResource(R.drawable.bg_selection);
        selectView(addText);
        Art text = new Art();
        text.setArtType(Art.TEXT);
        text.setText(label);
        text.setTextColor(selectedColor);
        text.setTextSize(textSize);
        selectedView.setTag(text);

    }

    float dX, dY;
    View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent event) {

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    dX = view.getX() - event.getRawX();
                    dY = view.getY() - event.getRawY();
                    if (view instanceof ImageView) {
                        if (selectedView != null) {
                            selectedView.setBackgroundResource(0);
                        }
                        selectedView = view;
                        selectedView.setBackgroundResource(R.drawable.bg_selection);
                        Art art = (Art) selectedView.getTag();

                        if (art.getArtType().equals(Art.SHAPE)) {
//                            showHolderShapeControl();
                            showHolderControl(holderShapeControls);
                            if (shapeListView.getVisibility() == View.VISIBLE) {
                                closeShapeChooserList();
                            }

                        } else {
                            showHolderControl(holderImageControls);
//                            showHolderImagecontrol();
                        }


                    } else if (view instanceof TextView) {

                        if (selectedView != null) {
                            selectedView.setBackgroundResource(0);
                        }
                        selectedView = view;
                        selectedView.setBackgroundResource(R.drawable.bg_selection);
                        showHolderControl(holderTextControls);
//                        showHolderImagecontrol();
                    }
                    break;

                case MotionEvent.ACTION_MOVE:
                    view.animate()
                            .x(event.getRawX() + dX)
                            .y(event.getRawY() + dY)
                            .setDuration(0)
                            .start();
                    break;


                default:
                    return false;
            }
            return true;
        }
    };

    private void showHolderShapeControl() {
        if (holderShapeControls.getVisibility() == View.GONE) {
            Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
            holderShapeControls.startAnimation(hideAnim);
            holderShapeControls.setVisibility(View.VISIBLE);
        }
        if (holderTextControls.getVisibility() == View.VISIBLE) {
            Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
            holderTextControls.startAnimation(hideAnim);
            holderTextControls.setVisibility(View.GONE);
        }

        if (holderImageControls.getVisibility() == View.VISIBLE) {
            Animation hideAnim3 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
            holderImageControls.startAnimation(hideAnim3);
            holderImageControls.setVisibility(View.GONE);
        }
    }

    private void showHolderTextControl() {


        if (selectedView != null && selectedView instanceof TextView) {
            float prog = ((TextView) selectedView).getTextSize();
//            sbTvSize.setProgress(((int) prog * 100) / 150);
        }
        if (holderTextControls.getVisibility() == View.GONE) {
            Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
            holderTextControls.startAnimation(hideAnim);
            holderTextControls.setVisibility(View.VISIBLE);
        }

        if (holderImageControls.getVisibility() == View.VISIBLE) {
            Animation hideAnim3 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
            holderImageControls.startAnimation(hideAnim3);
            holderImageControls.setVisibility(View.GONE);
        }
        if (holderShapeControls.getVisibility() == View.VISIBLE) {
            Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
            holderShapeControls.startAnimation(hideAnim);
            holderShapeControls.setVisibility(View.GONE);
        }
    }


    private void showHolderControl(LinearLayout holderControl) {
        switch (holderControl.getId()) {
            case R.id.holder_additional_controls_image:
                if (holderImageControls.getVisibility() == View.GONE) {
                    Animation hideAnim2 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
                    holderImageControls.startAnimation(hideAnim2);
                    holderImageControls.setVisibility(View.VISIBLE);
                }
                if (holderTextControls.getVisibility() == View.VISIBLE) {
                    Animation hideAnim3 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
                    holderTextControls.startAnimation(hideAnim3);
                    holderTextControls.setVisibility(View.GONE);
                }


                if (holderShapeControls.getVisibility() == View.VISIBLE) {
                    Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
                    holderShapeControls.startAnimation(hideAnim);
                    holderShapeControls.setVisibility(View.GONE);
                }

                if (selectedView != null) {
                    if (selectedView instanceof ImageView) {

                        Art art = (Art) selectedView.getTag();

                        int width = art.getImageWidth();
                        int height = art.getImageHeight();

                        Log.e("height b w", String.valueOf(height) + "  " + String.valueOf(width));
                        if (width == 200 || height == 200) {
                            sbImageSize.setProgress(50);
                        }
                        if (height > width) {
                            if (height < 200) {
                                sbImageSize.setProgress((int) Math.ceil((height / 4.06)));
                                Log.e("height", String.valueOf(height));
                            } else if (height > 200) {
                                sbImageSize.setProgress((int) Math.ceil((height / 4.06)));
                                Log.e("height", String.valueOf(height));
                            }
                        } else if (width == height) {
                            sbImageSize.setProgress((int) Math.ceil((height / 4.06)));
                        } else {
                            if (width > height) {
                                if (width < 200) {
                                    sbImageSize.setProgress((int) Math.ceil(width / 4.06));
                                    Log.e("height", String.valueOf(width));
                                } else if (width > 200) {
//
                                    sbImageSize.setProgress((int) Math.ceil(width / 4.06));
                                    Log.e("height", String.valueOf(width));
                                }
                            }
                        }


                    }

                }

                break;
            case R.id.holder_additional_controls_shape:
                if (holderShapeControls.getVisibility() == View.GONE) {
                    Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
                    holderShapeControls.startAnimation(hideAnim);
                    holderShapeControls.setVisibility(View.VISIBLE);
                }
                if (holderTextControls.getVisibility() == View.VISIBLE) {
                    Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
                    holderTextControls.startAnimation(hideAnim);
                    holderTextControls.setVisibility(View.GONE);
                }

                if (holderImageControls.getVisibility() == View.VISIBLE) {
                    Animation hideAnim3 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
                    holderImageControls.startAnimation(hideAnim3);
                    holderImageControls.setVisibility(View.GONE);
                }
                Log.e("shape", "here");

                if (selectedView != null) {
                    Log.e("shape", "there");
                    Art art = (Art) selectedView.getTag();
//                    if (art.getArtType() == Art.CLIP) {
                    int width = art.getImageWidth();
                    int height = art.getImageHeight();

                    Log.e("shape", height + "  " + width);
                    if (width == 5 || height == 5) {
                        sbShapeSize.setProgress(0);
                    } else if (width == 200 || height == 200) {
                        sbShapeSize.setProgress(50);
                        Log.e("shape", height + "  " + width);
                    } else {
                        Log.e("shape", height + "  " + width);
                        sbShapeSize.setProgress(height / 4);
                    }
//                    }
                }


                break;
            case R.id.holder_additional_controls_text:

                if (selectedView != null && selectedView instanceof TextView)

                {
                    Art art = (Art) selectedView.getTag();
                    sbTvSize.setProgress(art.getTextSize());
                }
                if (holderTextControls.getVisibility() == View.GONE)

                {
                    Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_up);
                    holderTextControls.startAnimation(hideAnim);
                    holderTextControls.setVisibility(View.VISIBLE);
                }

                if (holderImageControls.getVisibility() == View.VISIBLE)

                {
                    Animation hideAnim3 = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
                    holderImageControls.startAnimation(hideAnim3);
                    holderImageControls.setVisibility(View.GONE);
                }
                if (holderShapeControls.getVisibility() == View.VISIBLE)

                {
                    Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
                    holderShapeControls.startAnimation(hideAnim);
                    holderShapeControls.setVisibility(View.GONE);
                }
                break;
            default:
                break;
        }

    }

    private void hideHolderControl(final LinearLayout holderControl) {

        if (holderControl.getVisibility() == View.VISIBLE) {
            Animation hideAnim = AnimationUtils.loadAnimation(MainBoardLayout.this, R.anim.bottom_down);
            holderControl.startAnimation(hideAnim);
            hideAnim.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    holderControl.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        }
    }


    public void previewColorSelected(int color, View view) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.OVAL);
        shape.setColor(color);
        if (Build.VERSION.SDK_INT > 15) {
            view.setBackground(shape);
        } else {
            view.setBackgroundDrawable(shape);
        }
    }


    private void addImageView(int mindimention, String imagePath) {
        imageView = new ImageView(MainBoardLayout.this);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        imageView.setLayoutParams(lp);

        Art image = new Art();
        image.setArtType(Art.IMAGE);
        image.setImagepath(imagePath);
//        imageView.setTag(imagePath);
        imageView.setOnTouchListener(touchListener);
//        selectedImageView = imageView;
//        if (selectedImageView != null) {
//            Bitmap newBm = resizeHeightWidth(selectedImagePaths.get(selectedImageView.getId() - 1), mindimention);
//            selectedImageView.setImageBitmap(newBm);
//        }
//        mainBoard.addView(selectedImageView);
        if (selectedView != null) {
            selectedView.setBackgroundResource(0);
        }
        selectedView = imageView;
        selectedView.setBackgroundResource(R.drawable.bg_selection);
        Bitmap newBm = resizeHeightWidth(imagePath, mindimention);
        image.setImageHeight(newBm.getHeight());
        image.setImageWidth(newBm.getWidth());
        ((ImageView) selectedView).setImageBitmap(newBm);

        if (isBack) {
            backMainBoard.addView(selectedView);
        } else {
            mainBoard.addView(selectedView);
        }
        selectedView.setTag(image);
        selectView(addStickers);
    }

    private void addClip(int position) {
        imageView = new ImageView(MainBoardLayout.this);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.CENTER;
        imageView.setLayoutParams(lp);
        Picasso.with(MainBoardLayout.this).load(String.valueOf(clipList.get(position).getImagepath())).into(imageView);
//        imageView.setImageResource(clipAdapter.getItem(position));
        imageView.setOnTouchListener(touchListener);

        if (selectedView != null) {
            selectedView.setBackgroundResource(0);
        }
        selectedView = imageView;
        selectedView.setBackgroundResource(R.drawable.bg_selection);
//        selectedView.setTag(clipAdapter.getItem(position));
//        Bitmap bitmap = resizeHeightWidth(getResources(), Integer.parseInt(clipList.get(position).getImagepath()));

        Picasso.with(this)
                .load(clipList.get(position).getImagepath())
                .into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmaps, Picasso.LoadedFrom from) {
                        // loaded bitmap is here (bitmap)
                        Art clip = new Art();
                        final Bitmap bitmap = bitmaps;
                        int height = bitmap.getHeight();
                        int width = bitmap.getWidth();
                        ((ImageView) selectedView).setImageBitmap(bitmap);
                        clip.setArtType(Art.CLIP);
                        clip.setClip(bitmap);
                        clip.setImageHeight(height);
                        clip.setImageWidth(width);
                        selectedView.setTag(clip);
                        if (isBack) {
                            backMainBoard.addView(selectedView);
                        } else {
                            mainBoard.addView(selectedView);
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });


//        bitmap[0] = resizeClip(200, Integer.parseInt(clipList.get(position).getImagepath()), height, width);
//        bitmap = resizeHeightWidth( clipList.get(position).getImagepath(),200);


    }

    public static Bitmap resizeHeightWidth(String path, int minDimen) {
        Bitmap bitmap;
        // create the options
        BitmapFactory.Options opts = new BitmapFactory.Options();

        //just decode the file
        opts.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, opts);
        Log.e("path", path + " ");

        //get the original size
        int orignalHeight = opts.outHeight;
        int orignalWidth = opts.outWidth;

        Log.e("original height:width", orignalHeight + "::" + orignalWidth);

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        bitmap = BitmapFactory.decodeFile(path, options);

        float newHeight = orignalHeight;
        float newWidth = orignalWidth;
        //change height of image to 2000 if more than that
        if (orignalHeight > minDimen) {
            newHeight = minDimen;
            final float heightRatio = (float) orignalHeight / (float) minDimen;
            newWidth = newWidth / heightRatio;
            Log.e("newWidth for height 600", newWidth + " *" + "hei" + newHeight);

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
//            Log.e("resize height is greater with ratio :", heightRatio + "New Height: " + bitmap.getHeight()
//                    + " New Width: " + bitmap.getWidth() + " ");
        }
        if (newWidth > minDimen) {
            final float widthRatio = (float) newWidth / (float) minDimen;
            newWidth = minDimen;
            newHeight = newHeight / widthRatio;

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
            Log.e("newHeight for width 600", newWidth + " *" + "heig" + newHeight);

            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight());//, matrix, true);

            bitmap = Bitmap.createScaledBitmap(bitmap, (int) newWidth, (int) newHeight, true);
//            Log.e("resize width is greater with ratio :", " " + widthRatio + "New Height: " + bitmap.getHeight() + " New Width: " + bitmap.getWidth());
        }
        return bitmap;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Log.e("onActivity result", resultCode + "");
            if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
                addImageView(200, fileUri.getPath());

            } else if (requestCode == RESULT_LOAD_IMAGE) {
                //THE GALLERY APP WILL RETURN DATA IN INTENT (data) SO WE GET THAT DATA BY FOLLOWING PROCRESS
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                CursorLoader cursorLoader = new CursorLoader(MainBoardLayout.this, selectedImageUri, projection, null, null, null);
                Cursor cursor = cursorLoader.loadInBackground();
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();
                String selectedImagePath = cursor.getString(column_index);

                addImageView(200, selectedImagePath);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        Log.e("camera code1", requestCode + " cc");
        switch (requestCode) {
            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                Log.e("camera code2", CAMERA_CAPTURE_IMAGE_REQUEST_CODE + " cc");
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    //  task you need to do.
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                    startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case RESULT_LOAD_IMAGE: {
                Log.e("camera code3", CAMERA_CAPTURE_IMAGE_REQUEST_CODE + " cd");
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    //  task you need to do.
                    Intent i = new Intent(
                            Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, RESULT_LOAD_IMAGE);
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case DATA_STORAGE: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    new ImageLoadTask().execute();
                    Bitmap mockUp = getBitmapOfMockup(mainBoard);
                    String imageUri = saveBitmapToSdCard(mockUp);
                    Bitmap mockUp2 = getBitmapOfMockup(backMainBoard);
                    String imageUri2 = saveBitmapToSdCard(mockUp2);
                    Intent intent = new Intent(getApplicationContext(), PreviewActivity.class);
                    intent.putExtra("image", imageUri);
                    intent.putExtra("image_back", imageUri2);
                    startActivity(intent);
                } else {

                }
                return;


            }

        }
    }


    public Uri getOutputMediaFileUri(int i) {
        return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.e("getOutputMediaFile", "Oops! Failed create "
                        + IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
//        for type == MEDIA_TYPE_IMAGE) {
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");


        return mediaFile;
    }
}