package com.emts.customtshirtdesign;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 2017-02-16.
 */

public class ShirtListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
//    private int[] shirtsList = {R.drawable.shirt, R.drawable.shirt1, R.drawable.shirt2, R.drawable.shirt4, R.drawable.shirt5,
//            R.drawable.shirt6, R.drawable.shirt7, R.drawable.shirt8, R.drawable.shirt9, R.drawable.shirt10,
//            R.drawable.shirt11, R.drawable.shirt12, R.drawable.shirt13,
//            R.drawable.shirt14, R.drawable.shirt15, R.drawable.shirt16};
    ArrayList<Art>shirtsList;

    public ShirtListAdapter(Context context, ArrayList<Art> shirtsList) {
        this.context = context;
        this.shirtsList = shirtsList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_shirt_view, null, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Art art=shirtsList.get(position);
        Picasso.with(context).load(art.getFrontImagePath()).into(viewHolder.shirtsPreview);
//        viewHolder.shirtsPreview.setImageResource();
    }

//    public int getItem(int position) {
//        return shirtsList[position];
//    }

    @Override
    public int getItemCount() {
        return shirtsList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView shirtsPreview;

        ViewHolder(View itemView) {
            super(itemView);
            shirtsPreview = (ImageView) itemView.findViewById(R.id.item_image_prev);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clickListener.onRecyclerViewItemClick(getLayoutPosition());
                }
            });
        }
    }

    private OnRecyclerViewItemClicked clickListener;

    void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClicked clickListener) {
        this.clickListener = clickListener;
    }

    interface OnRecyclerViewItemClicked {
        void onRecyclerViewItemClick(int position);
    }
}
