package com.emts.customtshirtdesign;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

/**
 * Created by Srijana on 5/19/2017.
 */

public class ShapeAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private int[] shapeList = {R.drawable.square, R.drawable.rectangle, R.drawable.circle, R.drawable.oval, R.drawable.line};

    public ShapeAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_clip_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.shirtsPreview.setImageResource(shapeList[position]);
    }

    public int getItem(int position) {
        return shapeList[position];
    }

    @Override
    public int getItemCount() {
        return shapeList.length;
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView shirtsPreview;


        ViewHolder(View itemView) {
            super(itemView);
            shirtsPreview = (ImageView) itemView.findViewById(R.id.item_image_prev);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shapeClickListener.onRecyclerViewItemClick(getLayoutPosition());

                    Log.e("position", String.valueOf(getLayoutPosition()));
                }
            });
        }
    }

    private OnRecyclerViewItemClicked shapeClickListener;

    void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClicked clickListener) {
        this.shapeClickListener = clickListener;
    }

    interface OnRecyclerViewItemClicked {
        void onRecyclerViewItemClick(int position);
    }
}
