package com.emts.customtshirtdesign.helper;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 2017-01-06.
 */

public class VolleyHelper {
    public static final int STATUS_SITE_OFFLINE = 503;
    //    public static final int STATUS_AUTH_ERROR = 407;//Proxy Authentication Required
    public static final int STATUS_SESSION_OUT_ERROR = 407;//Proxy Authentication Required
    public static final int STATUS_AUTH_ERROR = 401;// Unauthorized
    public static final int STATUS_FORBIDDEN_ACCESS = 403;

    private Context context;
    static RequestQueue requestQueue;
    static VolleyHelper volleyHelper;

    private VolleyHelper(Context context) {
        this.context = context;
    }

    public static VolleyHelper getInstance(Context context) {
        if (volleyHelper != null) {
            return volleyHelper;
        } else {
            requestQueue = Volley.newRequestQueue(context);
            volleyHelper = new VolleyHelper(context);
            return volleyHelper;
        }

    }

    public void addVolleyRequestListeners(String url, int reqMethod, HashMap<String, String> postParams,
                                          VolleyHelperInterface callbacks, String requestTag) {
        addRequestToQueue(url, reqMethod, postParams, callbacks, requestTag);
    }

    public interface VolleyHelperInterface {
        void onSuccess(String response);

        void onError(String errorResponse, VolleyError volleyError);
    }

    private void addRequestToQueue(String url, final int reqMethod, final HashMap<String, String> postParams,
                                   final VolleyHelperInterface callbacks, final String requestTag) {
        Logger.e(requestTag + " url", url);

        StringRequest request = new StringRequest(reqMethod, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.e(requestTag + " response", response);
                if (callbacks != null) {
                    callbacks.onSuccess(response);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.e(requestTag + " error", error.getMessage() + "");
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    Logger.e(requestTag + " error res", errorObj.toString());
                    if (callbacks != null) {
                        callbacks.onError(errorObj.toString(), error);
                        return;
                    }
                } catch (Exception e) {
                    Logger.e("siteSettingTask error ex", e.getMessage() + "");
                }
                if (callbacks != null) {
                    callbacks.onError("", error);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (reqMethod == Method.POST && postParams != null) {
                    return postParams;
                }
                return super.getParams();
            }

        };
        RetryPolicy retryPolicy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        request.setTag(requestTag);

        requestQueue.add(request);
    }

    public void cancelRequest(String tag) {
        requestQueue.cancelAll(tag);
    }
}
