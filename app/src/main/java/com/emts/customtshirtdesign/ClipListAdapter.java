package com.emts.customtshirtdesign;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.emts.customtshirtdesign.helper.Logger;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Srijana on 5/17/2017.
 */

public class ClipListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
//    private int[] clipList = {R.drawable.ganesh, R.drawable.shiva, R.drawable.eagle,
//            R.drawable.peace,R.drawable.flag};

    ArrayList<Art> clipList = new ArrayList<>();


    public ClipListAdapter(Context context, ArrayList<Art> clipList) {
        this.context = context;
        this.clipList = clipList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_clip_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Art art = clipList.get(position);
        Logger.e("Clip",art.getImagepath());
        Picasso.with(context).load(art.getImagepath()).into(viewHolder.shirtsPreview);
    }

//    public int getItem(int position) {
//        return clipList[position];
//    }

    @Override
    public int getItemCount() {
        return clipList.size();
    }

    private class ViewHolder extends RecyclerView.ViewHolder {
        ImageView shirtsPreview;


        ViewHolder(View itemView) {
            super(itemView);
            shirtsPreview = (ImageView) itemView.findViewById(R.id.item_image_prev);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    clipClickListener.onRecyclerViewItemClick(getLayoutPosition());
                }
            });
        }
    }

    private OnRecyclerViewItemClicked clipClickListener;

    void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClicked clickListener) {
        this.clipClickListener = clickListener;
    }

    interface OnRecyclerViewItemClicked {
        void onRecyclerViewItemClick(int position);
    }
}
