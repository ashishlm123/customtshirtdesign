package com.emts.customtshirtdesign;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by User on 2017-02-07.
 */

public class MyDesignTable extends FrameLayout {
    private final String TAG = "MyDesignTable";

    public MyDesignTable(Context context) {
        this(context, null, 0);
    }

    public MyDesignTable(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyDesignTable(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        init(context, attrs, defStyleAttr);
    }

    Bitmap drawingCache;
    Canvas canvas;
    Paint paint;
    Bitmap bmShirt, bitmapSticker;
    Paint watermarkPaint;
    private Rect r = new Rect();
    String customLabel = "theone";
    int height, width;

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        if (attrs != null) {
        }
        drawingCache = Bitmap.createBitmap(300, 400, Bitmap.Config.ARGB_8888);

        canvas = new Canvas(drawingCache);
        paint = new Paint();
//        bmShirt = BitmapFactory.decodeResource(getResources(), R.drawable.shirt);
//        bmShirt = Bitmap.createScaledBitmap(bmShirt, getWidth(), getHeight(), false);
//        Log.e(TAG, "MyDesignTable height, width -->" + getHeight() + " " + getWidth());

//        bitmapSticker = BitmapFactory.decodeResource(getResources(), R.drawable.s_lion);
//        bitmapSticker = Bitmap.createScaledBitmap(bitmapSticker, 400, 200, false);

        watermarkPaint = new Paint();
        watermarkPaint.setColor(Color.RED);
        watermarkPaint.setAlpha(150);
        watermarkPaint.setTextSize(50);
        watermarkPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        Log.e(TAG, "init constructor");
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
        this.setMeasuredDimension(width, height);
        Log.e(TAG, "onMeasure first is it?" + width + " " + height);
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        width = w;
        height = h;
        Log.e(TAG, "onSizeChanged !!!" + w + " " + h);

        bmShirt = BitmapFactory.decodeResource(getResources(), R.drawable.shirt);
        bmShirt = Bitmap.createScaledBitmap(bmShirt, width - getPaddingLeft() - getPaddingRight(),
                height - getPaddingTop() - getPaddingBottom(), false);
        Log.e(TAG, "MyDesignTable height, width -->" + getHeight() + " " + getWidth());

        bitmapSticker = BitmapFactory.decodeResource(getResources(), R.drawable.s_lion);
        bitmapSticker = Bitmap.createScaledBitmap(bitmapSticker, 400, 200, false);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        Log.e(TAG, "onFinishInflate !!!");
    }

    @Override
    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
        if (bmShirt != null) {

//      //Draw your bmShirt to the canvas
            canvas.drawBitmap(bmShirt, null, new RectF(0, 0, width - getPaddingLeft() - getPaddingRight(),
                    height - getPaddingTop() - getPaddingBottom()), null);

            drawCenter(canvas, watermarkPaint, customLabel);

//        //draw the sticker
//        int centreX = (canvas.getWidth() - bitmapSticker.getWidth()) / 2;
//
//        int centreY = (canvas.getHeight() - bitmapSticker.getHeight()) / 2;
//        canvas.drawBitmap(bitmapSticker, null, new RectF(centreX, centreY, centreX + 400, centreY + 200), null);
//        canvas.drawBitmap(bitmapSticker, centreX, centreY, null);

        }
    }

//    @Override
//    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
//        super.onLayout(changed, left, top, right, bottom);
//
//        int childCount = getChildCount();
//
//        for (int i = 0; i<childCount; i++){
//            View child = getChildAt(i);
//            child.layout(left, top, right, bottom);
//        }
//    }

    private void drawCenter(Canvas canvas, Paint paint, String text) {
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
        float y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);
    }

    public void addCustomLabel(String label) {
        this.customLabel = label;
        invalidate();
    }
}
