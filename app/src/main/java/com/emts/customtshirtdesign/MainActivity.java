package com.emts.customtshirtdesign;

import android.content.DialogInterface;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView addText, addStickers, pickColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final MyDesignTable customShirt = (MyDesignTable) findViewById(R.id.custom_shirt);
        addText = (ImageView) findViewById(R.id.add_text);
        addStickers = (ImageView) findViewById(R.id.add_image);
        pickColor = (ImageView) findViewById(R.id.add_color);

        addText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectView(view);
                AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
                alert.setTitle("Enter text to add to image");
                final EditText etCustomText = new EditText(MainActivity.this);
                alert.setView(etCustomText);
                alert.setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (!TextUtils.isEmpty(etCustomText.getText().toString().trim())) {
                            customShirt.addCustomLabel(etCustomText.getText().toString().trim());
                        }
                    }
                });
                alert.show();
            }
        });
        addStickers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectView(view);
            }
        });
        pickColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectView(view);
            }
        });
    }

    public void selectView(View view){
        switch(view.getId()){
            case R.id.add_text:
                addText.setBackgroundColor(Color.GREEN);

                addStickers.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                pickColor.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                break;
            case R.id.add_image:
                addStickers.setBackgroundColor(Color.GREEN);

                addText.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                pickColor.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                break;
            case R.id.add_color:
                pickColor.setBackgroundColor(Color.GREEN);

                addText.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                addStickers.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                break;
        }
    }
}
