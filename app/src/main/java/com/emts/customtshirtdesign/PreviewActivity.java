package com.emts.customtshirtdesign;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import java.io.File;

public class PreviewActivity extends AppCompatActivity {
    String frontView = "";
    String rearView = "";
    Button btnOrderNow;
    EditText edtEmail, edtFullName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);

        frontView = getIntent().getStringExtra("front_view");
        rearView = getIntent().getStringExtra("rear_view");
        Log.e("imageUrl", frontView);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Preview");
        toolbar.inflateMenu(R.menu.menu_main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                PreviewActivity.super.onBackPressed();
                return false;
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        edtEmail = findViewById(R.id.edt_email);
        edtFullName = findViewById(R.id.edt_name);

        btnOrderNow = (Button) findViewById(R.id.btn_order_now);
        btnOrderNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(PreviewActivity.this, OrderListingActivity.class);
                intent.putExtra("front_img", frontView);
                intent.putExtra("back_img", rearView);
                intent.putExtra("email", edtEmail.getText().toString().trim());
                intent.putExtra("full_name", edtFullName.getText().toString().trim());
            }
        });

        ImageView previewView = (ImageView) findViewById(R.id.preview_image);
        ImageView backPreview = (ImageView) findViewById(R.id.preview_image_back);
        previewView.setImageBitmap(BitmapFactory.decodeFile(frontView));
        backPreview.setImageBitmap(BitmapFactory.decodeFile(rearView));
    }

    @Override
    public void onBackPressed() {
        File file = new File(frontView);
        if (file.exists()) {
            file.delete();
        }
        super.onBackPressed();
    }
}
